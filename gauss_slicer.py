# trace generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
from paraview.vtk.numpy_interface import dataset_adapter as dsa
from paraview import servermanager as sm
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

points = np.loadtxt("streanline_points.csv")
normals = np.loadtxt("streamline_normals.csv")

def get_temperature_profile(input_path):
    vtk_reader = LegacyVTKReader(registrationName='name', FileNames=[input_path])

    UpdatePipeline(time=0.0, proxy=vtk_reader)


    WIDTH = 150

    temps = []
    points_out = []

    for point, normal in zip(points,normals):
        normal[2] = 0
        normal = normal/np.sqrt(np.sum(normal*normal))
        left = point+normal*WIDTH/2
        right = point-normal*WIDTH/2

        # init the 'Line' selected for 'Source'
        plotOverLine = PlotOverLine(registrationName='PlotOverLine', Input=vtk_reader,
            Source='Line')
        plotOverLine.Source.Point1 = left
        plotOverLine.Source.Point2 = right

    #    UpdatePipeline(time=0.0, proxy=plotOverLine)

        # toggle 3D widget visibility (only when running from the GUI)
        # Hide3DWidgets(proxy=plotOverLine.Source)
        vtk_data = sm.Fetch(plotOverLine)
        vtk_data = dsa.WrapDataObject(vtk_data)
        temp = vtk_data.PointData.GetArray('Temperature')
        temps.append(np.array(temp))
        points_out.append(vtk_data.Points)

    np.savetxt(input_path[:-3] + "gauss",np.array(temps))

dir = sys.argv[1]
folders = list(os.listdir(dir))
for n,folder in enumerate(folders):
    print(f"{n+1}/{len(folders)}")
    ResetSession()
    folder_path = os.path.join(dir,folder)
    if os.path.isdir(folder_path):
        for file_name in os.listdir(folder_path):
            if file_name.endswith(".vtk") and not os.path.exists(os.path.join(folder_path,file_name[:-3]+"gauss")):
                print(file_name)
                get_temperature_profile(os.path.join(folder_path,file_name))