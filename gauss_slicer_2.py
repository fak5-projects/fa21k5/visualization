import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import meshio
from tqdm.auto import tqdm

def get_temperature_profile(input_path):
    mesh = meshio.read(input_path)
    temps = mesh.cell_data['Temperature'][0].reshape((70,150))
    np.savetxt(input_path[:-3] + "gauss2",np.array(temps))

dir = sys.argv[1]
folders = list(os.listdir(dir))
for n,folder in tqdm(list(enumerate(folders))):
    # print(f"{n+1}/{len(folders)}")
    folder_path = os.path.join(dir,folder)
    if os.path.isdir(folder_path):
        for file_name in os.listdir(folder_path):
            if file_name.endswith(".vtk") and not os.path.exists(os.path.join(folder_path,file_name[:-3]+"gauss2")):
                # print(file_name)
                get_temperature_profile(os.path.join(folder_path,file_name))