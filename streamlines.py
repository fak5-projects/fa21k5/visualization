# import the simple module from the paraview
from paraview.simple import *
import vtk
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np
import os
import argparse

TEMP_PATH = "tmp.vtk"

def extract_streamline_coordinates(input_path, output_path):
    """
    Extract the streamlines from a vtk file and save it to a csv file
    Extracted data is positions and normals

    Parameters
    ----------
        input_path: path to the streamline vtk file
        output_path: the path to output the csv file to
    """
    # set up the reader
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(input_path)
    reader.ReadAllScalarsOn()
    reader.ReadAllVectorsOn()
    reader.ReadAllTensorsOn()
    reader.Update()

    # actually read
    vtkdata = reader.GetOutput()
    wrapped_data = dsa.WrapDataObject(vtkdata)
    points = np.array(wrapped_data.Points)
    vtk_lines = vtkdata.GetLines()
    data = vtk_lines.GetOffsetsArray()
    offsets = np.array(dsa.VTKArray(data))
    normals = np.array(wrapped_data.PointData.GetArray('Normals'))
    lines_positions = []
    lines_normals = []

    # seperate streamlines
    prev = 0
    for o in offsets[1:]:
        lines_positions.append(points[prev:o])
        lines_normals.append(normals[prev:o])
        prev = o

    # now save
    with open(output_path, "w") as file:
        for n,(line_positions,line_normals) in enumerate(zip(lines_positions,lines_normals)):
            file.write(f"positions {n}\n")
            for x, y, z in line_positions:
                file.write(f"{x} {y} {z}\n")
            file.write("\n")
            file.write(f"normals {n}\n")
            for x, y, z in line_normals:
                file.write(f"{x} {y} {z}\n")
            file.write("\n")


def create_streamlines(points_path, input_paths, output_path):

    # coordinates of streamlines (x,y,z)
    startpoints = np.loadtxt(points_path)

    # load data
    data = LegacyVTKReader(registrationName='data', FileNames=input_paths)

    # get animation scene
    animationScene1 = GetAnimationScene()
    # update animation scene based on data timesteps
    animationScene1.UpdateAnimationUsingDataTimeSteps()
    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # update the view to ensure updated data information
    renderView1.Update()

    # Cell Data to Point Data filter
    cellDatatoPointData1 = CellDatatoPointData(
        registrationName='CellDatatoPointData1', Input=data)
    cellDatatoPointData1.CellDataArraytoprocess = [
        'Material_ID', 'Vlx', 'Vly', 'Vlz']

    # Calculator filter
    calculator1 = Calculator(registrationName='Calculator1',
                             Input=cellDatatoPointData1)
    calculator1.ResultArrayName = 'velocity'
    calculator1.Function = 'iHat*Vlx+jHat*Vly+kHat*Vlz'

    # Poly point cloud source
    polyPointSource1 = PolyPointSource(registrationName='PolyPointSource1')
    polyPointSource1.Points = startpoints.reshape(-1)

    # Stream Tracer filter with custom source
    streamTracerWithCustomSource1 = StreamTracerWithCustomSource(
        registrationName='StreamTracerWithCustomSource1', Input=calculator1, SeedSource=polyPointSource1)
    streamTracerWithCustomSource1.Vectors = ['POINTS', 'velocity']
    streamTracerWithCustomSource1.MaximumStreamlineLength = 1000.0

    # get active source.
    streamTracerWithCustomSource1 = GetActiveSource()

    # animation
    animationScene1 = GetAnimationScene()
    animationScene1.GoToNext()

    # save data
    SaveData(output_path,
             proxy=streamTracerWithCustomSource1, ChooseArraysToWrite=1, FileType='Ascii')


def streamlines_from_vtks(points_path, input_paths, output_path):
    create_streamlines(points_path, input_paths, TEMP_PATH)
    extract_streamline_coordinates(TEMP_PATH, output_path)
    os.remove(TEMP_PATH)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Take some vtks vector fields, receive streamline coordinates.')
    parser.add_argument("input_files", nargs="+", help="the input files")
    parser.add_argument("-points", metavar="p", type=str,
                        help="csv file containing the points where the streamlines should start.")
    parser.add_argument("-out", metavar='o',
                        help="the location of the output file.")
    args = parser.parse_args()
    streamlines_from_vtks(args.points, args.input_files, args.out)
