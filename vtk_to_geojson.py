import numpy as np
import meshio
import geojson
import datetime
from tqdm.auto import tqdm
import os
import argparse


def cell_points_to_map_coordinates(cell_points, conversion, verbose=True):
    '''
    Convert x and y coordinates of cell vertices to longitude and latitude (in place)

    Parameters
    ----------
        cell_points: array of cells where each cell is an array of ordered [x,y,z] coordinates
        conversion = (lat_min, lat_max), (lon_min, lon_max)
        verbose: print progress bars if True

    Return
    ------
        None
    '''

    # Calculate the dimesions for rescaling
    x_max, y_max, z_max = np.max(np.max(cell_points, axis=1), axis=0)
    x_min, y_min, z_min = np.min(np.min(cell_points, axis=1), axis=0)

    (lat_min, lat_max), (lon_min, lon_max) = conversion
    n_cells, n_vertices, _ = cell_points.shape

    # TODO optimize if nessesary
    for i in tqdm(list(range(n_cells)), desc="Map coordinates", disable=not verbose):
        for k in range(n_vertices):
            cell_points[i, k, 0] = (
                cell_points[i, k, 0]-x_min) / (x_max-x_min) * (lat_max-lat_min) + lat_min
            cell_points[i, k, 1] = (
                cell_points[i, k, 1]-y_min) / (y_max-y_min) * (lon_max-lon_min) + lon_min


def build_feature_list(input_file, conversion=((48.1, 48.14), (11.5, 11.52)), date=datetime.datetime.now(), verbose=True, variables=None):
    """
    Build a list of geojson features from a vtk file

    Parameters
    ----------
        input_file: path to input vtk file
        conversion: (lat_min, lat_max), (lon_min, lon_max)
        verbose: print progress bars if True

    Return
    ------
        a list of geojson features
    """
    # read input
    mesh = meshio.read(input_file)

    # evaluate the mesh data: replace the vertex indices by the coordinates
    cell_points = np.array([[mesh.points[index] for index in indices]
                            for indices in mesh.cells[0].data], dtype=np.float64)

    # convert the sim coordinates to latitude and longitude
    cell_points_to_map_coordinates(cell_points, conversion, verbose)

    feature_list = []

    for cell, index in tqdm(list(zip(cell_points, range(len(cell_points)))), desc="Create Polygons", disable=not verbose):
        z_dict = {}
        # "sort" the vertices by depth
        for x, y, z in cell:
            if z not in z_dict:
                z_dict[z] = []
            z_dict[z].append([y, x])

        # create a polygon from the vertices of the same depth for each cell
        for depth, value in z_dict.items():

            # calculate the center of the cell
            lat = 0
            lon = 0
            n = 0
            for x, y in value:
                lat += y
                lon += x
                n += 1
            lat /= n
            lon /= n

            polygon = geojson.Polygon([value])
            properties = {"datetime": date.isoformat(), "depth": depth,
                          "Lat": lat, "Lon": lon}
        
            if not variables:
                variables = mesh.cell_data.keys()
            for variable in variables:
                properties[variable] = float(
                    mesh.cell_data[variable][0][index][0])

            # wrap the polygon in a feature to give it additional properties
            feature = geojson.Feature(geometry=polygon, properties=properties)
            feature_list.append(feature)
    return feature_list


def file_to_geojson(input_file, output_file=None, conversion=((48.1, 48.14), (11.5, 11.52)), date=datetime.datetime.now(), verbose=True, variables=None):
    """
    Convert a single vtk file into a geojson file by mapping to the given coordinates
    Output contains polygons and points
    Parameters
    ----------
        input_file: path to the input vtk file
        output_file: path for the finished geojson file, will be overwritten
                     defaults to input_file.geojson
        conversion: (lat_min, lat_max), (lon_min, lon_max)

    Return
    ------
        output_path
    """

    feature_list = build_feature_list(
        input_file, conversion, date, verbose, variables)
    feature_collection = geojson.FeatureCollection(feature_list)
    json = geojson.dumps(feature_collection)

    if not output_file:
        output_file = input_file[:-4] + ".geojson"

    with open(output_file, "w") as file:
        file.write(json)

    return output_file


def files_to_geojson(file_paths, output_file=None, conversion=((48.1, 48.14), (11.5, 11.52)), date_range=(datetime.datetime.now(), datetime.datetime.now()+datetime.timedelta(1)), verbose=True, variables=None):
    """
    Convert all vtks of the given file_paths to a timeseries geojson

    Parameters
    ----------
        file_paths: list of paths to the vtks
        output_file: path to the output file
                     defaults to input_folder/input_folder.geojson
        conversion: (lat_min, lat_max), (lon_min, lon_max)
        date_range: (start_datetime, end_datetime), files get mapped to this date range with linear spacing

    Return
    ------
        output_path
    """

    n_files = len(file_paths)
    assert n_files > 1, "provide more than one vtk file in the folder"

    start_datetime, end_datetime = date_range
    time_delta = (end_datetime-start_datetime)/(n_files-1)

    feature_list = []

    for n, file in enumerate(file_paths):
        print(f"Convert {n+1} of {n_files}")
        date = start_datetime + time_delta*n
        feature_list += build_feature_list(file,
                                           conversion, date, verbose, variables)

    feature_collection = geojson.FeatureCollection(feature_list)
    json = geojson.dumps(feature_collection)

    if not output_file:
        output_file = file_paths[0][:-4] + ".geojson"

    with open(output_file, "w") as file:
        file.write(json)

    return output_file


def folder_to_geojson(input_folder, output_file=None, conversion=((48.1, 48.14), (11.5, 11.52)), date_range=(datetime.datetime.now(), datetime.datetime.now()+datetime.timedelta(1)), verbose=True, variables=None):
    """
    Convert all vtks in input_folder to a timeseries geojson

    Parameters
    ----------
        input_folder: path to the folder containing the vtks
        output_file: path to the output file
                     defaults to input_folder/input_folder.geojson
        conversion: (lat_min, lat_max), (lon_min, lon_max)
        date_range: (start_datetime, end_datetime), files get mapped to this date range with linear spacing

    Return
    ------
        output_path
    """

    # get all vtk files
    file_paths = list(
        map(lambda file_name: os.path.join(input_folder, file_name),
            filter(lambda name: name.endswith("vtk"),
                   os.listdir(input_folder))))

    if not output_file:
        output_file = os.path.join(input_folder, input_folder + ".geojson")

    return files_to_geojson(file_paths, output_file, conversion, date_range, verbose,variables)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Convert vtk files to a single geojson.')
    parser.add_argument('files', nargs='+',
                        help='either a single file, a list of files or a folder containing all files that need to be converted')
    parser.add_argument('-out', metavar='o', default=None,
                        help="the output path. default is input.geojson")
    parser.add_argument('-coords', metavar='c', nargs=4, type=float, default=(48.1, 48.14, 11.5, 11.52),
                        help="the coordinates to map to. lat_min lat_max lon_min lon_max")
    parser.add_argument('-verbose', metavar='v', default=True)
    parser.add_argument('-dates', metavar='d', nargs=2, type=datetime.datetime.fromisoformat,
                        help='start and end date of the input files. format is YYYY-MM-DD or YYYY-MM-DDTHH:mm',
                        default=(datetime.datetime.now(), datetime.datetime.now()+datetime.timedelta(1)))
    parser.add_argument('-vars', nargs='*', type=str, default=None,
                        help='the variables to put in the geojson file, default is all')
    parser.add_argument(
        '-depth', help="depth layers to include, defaults to all", type=float, nargs='*')
    args = parser.parse_args()

    lat_min, lat_max, lon_min, lon_max = args.coords

    status = "failed"

    if len(args.files) == 1:
        path = args.files[0]
        if os.path.isfile(path):
            status = file_to_geojson(path, output_file=args.out, conversion=(
                (lat_min, lat_max), (lon_min, lon_max)), date=args.dates[0], verbose=args.verbose,variables=args.vars)
        elif os.path.isdir(path):
            status = folder_to_geojson(path, output_file=args.out, conversion=(
                (lat_min, lat_max), (lon_min, lon_max)), date_range=args.dates, verbose=args.verbose,variables = args.vars)
    else:
        status = files_to_geojson(args.files, output_file=args.out, conversion=(
            (lat_min, lat_max), (lon_min, lon_max)), date_range=args.dates, verbose=args.verbose,variables = args.vars)
    input(f"Saved result to {status}\n")
